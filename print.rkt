#lang curly-fn racket/base

(require
 conllu/type
 racket/require
 (multi-in racket [format list string match])
 )

(provide
 format-id
 format-sents
 format-sent
 format-token)

(define/match (format-id id)
  [((? number?)) (~a id)]
  [((list 'multi s e)) (~a s #\- e)]
  [((list 'empty s e)) (~a s #\. e)]
  [(_) (error
        'format-id
        "ID ~s doesn't match integer or list of either 'multi or empty and two integers." id)])

(define (format-form f)
  f)

(define (format-lemma l)
  l)

(define (format-upos up)
  (or up "_"))

(define (format-xpos xp)
  (format-upos xp))

(define (format-list lst [intra-elem-sep "="])
  (define (print-field f)
    (~a (car f) intra-elem-sep (string-join (rest f) ",")))
  (or-empty lst (lambda (lst) (apply ~a (map print-field lst) #:separator "|"))))

(define (format-feats fs)
  (format-list fs))

(define (format-head h)
  (or-empty h format-id))

(define (or-empty e pfn)
  (if e
      (pfn e)
      "_"))

(define (format-deprel d)
  (or-empty d #{string-join % ":"}))

(define (format-deps ds)
  (define (format-dep d)
    (string-join (cons (format-id (first d)) (rest d)) ":"))
  (or-empty ds (lambda (ds) (string-join (map format-dep ds) "|"))))

(define (format-misc m)
  (or-empty m #{string-join % "|"}))


(define (format-token tk)
  (let ([ids (format-id     (token-id tk))]
        [f   (format-form   (token-form tk))]
        [l   (format-lemma  (token-lemma tk))]
        [up  (format-upos   (token-upos tk))]
        [xp  (format-xpos   (token-xpos tk))]
        [fs  (format-feats  (token-feats tk))]
        [h   (format-head   (token-head tk))]
        [d   (format-deprel (token-deprel tk))]
        [ds  (format-deps   (token-deps tk))]
        [m   (format-misc   (token-misc tk))])
    (~a ids f l up xp fs h d ds m #:separator "\t")))

(define (format-comments cs)
  (define (go c)
    (string-join c " = " #:before-first "# "))
  (map go cs))

(define (format-sent sent)
  (string-join
   (append
    (format-comments
     (sent-comments sent))
    (map format-token (sent-tokens sent)))
   "\n" #:after-last "\n"))

(define (format-sents ss)
  (string-join
   (map format-sent ss)
   "\n" #:after-last "\n"))
